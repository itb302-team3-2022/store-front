import { createModel } from '@rematch/core'
import API from 'api'
import { NewShipmentData } from '@/utils/common/api/Request/types'
import {
  Customer,
  EnumMessageType,
  Order,
  Product,
  Team3StoreState
} from 'utils/common/type'
import type { RootModel } from '.'

export const team3Store = createModel<RootModel>()({
  state: {
    orders: [],
    products: [],
    customers: [],
  } as Team3StoreState,
  reducers: {
    setOrders(state, orders: Order[]) {
      return { ...state, orders }
    },
    setCustomers(state, customers: Customer[]) {
      return { ...state, customers }
    },
    setProducts(state, products: Product[]) {
      return { ...state, products }
    },
  },
  effects: (dispatch) => ({
    async fetchAllOrders() {
      try {
        const { data, auth } = await API.fetchAllOrders()
        dispatch.team3Store.setOrders(data)
        dispatch.app.setStaff(auth)
      } catch (err) {
        console.log(err)
      }
    },
    async fetchAllProducts() {
      try {
        const { data, auth } = await API.fetchAllProducts()
        dispatch.team3Store.setProducts(data)
        dispatch.app.setStaff(auth)
      } catch (err) {
        console.log(err)
      }
    },
    async fetchAllCustomers() {
      try {
        const { data, auth } = await API.fetchAllCustomers()
        dispatch.team3Store.setCustomers(data)
        dispatch.app.setStaff(auth)
      } catch (err) {
        console.log(err)
      }
    },
    async fetchAll() {
      dispatch.team3Store.fetchAllCustomers()
      dispatch.team3Store.fetchAllOrders()
      dispatch.team3Store.fetchAllProducts()
    },
    async createNewShipment(data: NewShipmentData): Promise<void> {
      return new Promise(async (resolve, reject) => {
        try {
          await API.createNewShipment(data)
          dispatch.app.setAlertMessageAsync({
            type: EnumMessageType.Success,
            text: 'Shipment Create Success',
          })
          dispatch.team3Store.fetchAllOrders()
          resolve()
        } catch (err) {
          dispatch.app.setAlertMessageAsync({
            type: EnumMessageType.Fail,
            text: 'Unable to create shipment',
          })
          console.log(err)
          reject(err)
        }
      })
    },
  }),
  //   selectors: (slice, createSelector, hasProps) => ({
  //   })
})
