import { Models } from '@rematch/core'
import { team3Store } from './team3Store'
import { app } from './app'

export interface RootModel extends Models<RootModel> {
  team3Store: typeof team3Store
  app: typeof app
}

export const models: RootModel = { team3Store, app }
