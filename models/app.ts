import API from '@/api'
import { LoginData } from '@/utils/common/api/Request/types'
import { createModel } from '@rematch/core'
import {
  AlertMessage,
  AppState,
  EnumMessageType,
  Staff,
} from 'utils/common/type'
import type { RootModel } from '.'

export const app = createModel<RootModel>()({
  state: {
    alertMessage: null,
    staff: null,
  } as AppState,
  reducers: {
    setAlertMessage(state, alertMessage: AlertMessage) {
      return { ...state, alertMessage }
    },
    clearAlertMessage(state) {
      return { ...state, alertMessage: null }
    },
    setStaff(state, staff: Staff) {
      return { ...state, staff }
    },
  },
  effects: (dispatch) => ({
    login(data: LoginData): Promise<void> {
      return new Promise(async (resolve, reject) => {
        try {
          const resp = await API.login(data)
          resolve()
        } catch (err) {
          dispatch.app.setAlertMessageAsync({
            type: EnumMessageType.Fail,
            text: 'Invalid username or password',
          })
          reject(err)
        }
      })
    },

    setAlertMessageAsync(message: AlertMessage) {
      dispatch.app.setAlertMessage(message)
      setTimeout(() => {
        dispatch.app.clearAlertMessage()
      }, 3000)
    },
  }),
  //   selectors: (slice, createSelector, hasProps) => ({
  //   })
})
