import { init, RematchDispatch, RematchRootState } from "@rematch/core";
import selectPlugin from "@rematch/select";
import { models, RootModel } from "../models";

export const store = init<RootModel>({
  models,
  redux: {
    devtoolOptions: {
      disabled: false,
    },
  },
  plugins: [selectPlugin()],
});

export const { select } = store;
export type Store = typeof store;
export type Dispatch = RematchDispatch<RootModel>;
export type RootState = RematchRootState<RootModel>;
