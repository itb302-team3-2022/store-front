import axios from 'axios'
import { Methods } from 'utils/common/api'

const baseURL = 'http://team3store.com:3005' + '/api'

const customAxios = axios.create({
  baseURL,
  withCredentials: true,
})

customAxios.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    // whatever you want to do with the error
    if (error.response.status === 401) {
      location.replace('/login')
    }
    throw error
  }
)

const request = (method: Methods, url: string, data?: any): Promise<any> => {
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await customAxios({
        method,
        data,
        url,
      })
      resolve(resp.data)
    } catch (err) {
      reject()
    }
  })
}

export default request
