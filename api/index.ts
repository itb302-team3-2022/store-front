import { LoginData, NewShipmentData } from '@/utils/common/api/Request/types'
import { Methods } from 'utils/common/api'
import request from './request'

const API = {
  login(data: LoginData) {
    return request(Methods.Post, '/staff/login', data)
  },
  fetchAllOrders() {
    return request(Methods.Get, '/orders')
  },
  fetchAllCustomers() {
    return request(Methods.Get, '/customers')
  },
  fetchAllProducts() {
    return request(Methods.Get, '/products')
  },
  fetchOrderById(productId: string) {
    return request(Methods.Get, `/order/?id=${productId}`)
  },
  createNewShipment(data: NewShipmentData) {
    return request(Methods.Post, `/ship`, data)
  },
}

export default API
