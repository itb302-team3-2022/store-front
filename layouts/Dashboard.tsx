import { store } from '@/store'
import { FooterAdmin, NavbarAdmin, Sidebar } from 'components'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { Provider } from 'react-redux'

type Props = {
  children: React.ReactNode
}

const Dashboard: React.FC<Props> = ({ children }) => {
  return (
    <>
      <Sidebar />
      <div className="relative md:ml-64">
        <NavbarAdmin />
        {/* Header */}
        {/* <HeaderStats /> */}
        {/* <div className="px-4 md:px-10 mx-auto w-full -m-24">{children}</div> */}
        <div className="w-full h-screen flex flex-col justify-between pt-20 px-4  bg-white">
          {/* <div className="px-4 md:px-10 w-full pt-20 bg-blueGray-100">
          </div> */}
          {children}

          <FooterAdmin />
        </div>
      </div>
    </>
  )
}

export default Dashboard
