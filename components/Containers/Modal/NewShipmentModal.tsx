import { Modal } from '@/components'
import { Dispatch } from '@/store'
import { NewShipmentData } from '@/utils/common/api/Request/types'
import {
  EnumDimensionUnit,
  EnumMessageType,
  EnumModalAnimate,
  EnumWeightUnit,
  Order,
  Parcel,
} from '@/utils/common/type'
import { DEFAULT_SHIP_FROM } from '@/utils/const'
import { faWarning } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import dayjs from 'dayjs'
import { useEffect, useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import styles from './Common.module.scss'
import OrderDetail from './OrderDetail'

type Props = {
  order: Order
  onClose: (e?: any) => void
}

type PickUpStr = {
  startTime: dayjs.Dayjs
  endTime: dayjs.Dayjs
}

const NewShipmentModal: React.FC<Props> = ({ order, onClose }) => {
  // const [error, setError] = useState<string>('')
  const parcelEl = useRef<HTMLTableRowElement>(null)
  const [shipFrom, setShipFrom] =
    useState<typeof DEFAULT_SHIP_FROM>(DEFAULT_SHIP_FROM)

  const [pickUp, setPickUp] = useState<PickUpStr>({
    startTime: dayjs(),
    endTime: dayjs().add(5, 'day'),
  })

  const [parcels, setParcels] = useState<Parcel[]>([])

  const dispatch = useDispatch<Dispatch>()

  const onSubmit = async () => {
    // combine all inputs
    if (parcels.length === 0) {
      dispatch.app.setAlertMessageAsync({
        type: EnumMessageType.Warning,
        text: 'Please enter at least one parcel with Reference ID'
      })
      return
    }

    const _pickUp = {
      startTime: dayjs(pickUp.startTime, 'DD-MM-YYYY').unix() * 1000,
      endTime: dayjs(pickUp.endTime, 'DD-MM-YYYY').unix() * 1000,
    }

    const shipmentData: NewShipmentData = {
      orderId: order._id,
      parcels,
      shipFrom,
      pickUp: _pickUp,
    }

    try {
      await dispatch.team3Store.createNewShipment(shipmentData)
      onClose()
    } catch {}
  }

  const onParcelDelete = (i: number) => {
    const updates = parcels.filter((p, _i) => _i !== i)
    setParcels(updates)
  }
  

  const onParcelAdd = () => {
    const elements = parcelEl.current?.children as any
    const newParcel: any = {}

    if (!elements) {
      return
    }

    for (let el of elements) {
      const targetElement = el.children[0]
      if (targetElement.tagName === 'INPUT') {
        if (targetElement.id === 'parcelId') {
          newParcel['refId'] = targetElement.value
        } else if (targetElement.id === 'weight') {
          newParcel['weight'] = {
            value: targetElement.value,
            uint: EnumWeightUnit.Kg,
          }
        } else {
          newParcel['dimension'] = {
            ...newParcel['dimension'],
            [targetElement.id]: targetElement.value,
            unit: EnumDimensionUnit.Cm,
          }
        }
      }
    }

    // simple validation
    if (!newParcel['refId']) {
      return
    }

    // reset
    for (let el of elements) {
      const targetElement = el.children[0]
      if (targetElement.tagName === 'INPUT') {
        targetElement.value = ''
      }
    }
    setParcels((prev) => [...prev, newParcel])
  }

  // useEffect(() => {
  //   if (error) {
  //     setTimeout(() => {
  //       setError('')
  //     }, 3000)
  //   }
  // }, [error])

  return (
    <Modal
      backdrop
      title="Create Shipment"
      animateStyle={EnumModalAnimate.FromBottom}
      onClose={onClose}
    >
      <div
        className="w-full sm:w-96 p-5 flex flex-col space-y-5"
        style={{ width: '80vw', height: '80vh' }}
      >
        <div>
          <h2 className="text-3xl text-gray-400 text-center">New Shipment</h2>
          <div className={styles.split}>
            <div className={styles.col}>
              <div className={styles.row}>
                <div className={styles.rowTitle}>Provider</div>
                <div className={styles.rowValue}>
                  <select
                    defaultChecked
                    className="select select-ghost w-full max-w-xs"
                  >
                    <option value="DEFAULT">SL Express</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className={styles.split}>
            <div className={styles.col}>
              <div className={styles.row}>
                <div className={`${styles.rowTitle} ${styles.single}`}>
                  PickUp From
                </div>
              </div>
              <div className={styles.row}>
                <div
                  className={`${styles.rowValue} ${styles.single} space-y-2 p-2`}
                >
                  <div className="">
                    <span className="text-sm text-gray-500">Reciever</span>
                    <input
                      type="text"
                      placeholder="Contact Person"
                      value={shipFrom.name}
                      onChange={(e) =>
                        setShipFrom((prev) => ({
                          ...prev,
                          name: e.target.value,
                        }))
                      }
                      className="input input-bordered w-full"
                    />
                  </div>
                  <div className="">
                    <span className="text-sm text-gray-500">Phone</span>
                    <input
                      type="text"
                      placeholder="Phone"
                      value={shipFrom.phone}
                      onChange={(e) =>
                        setShipFrom((prev) => ({
                          ...prev,
                          phone: e.target.value,
                        }))
                      }
                      className="input input-bordered w-full"
                    />
                  </div>
                  <div className="">
                    <span className="text-sm text-gray-500">Address</span>
                    <input
                      type="text"
                      placeholder="Pickup Address"
                      value={shipFrom.address}
                      onChange={(e) =>
                        setShipFrom((prev) => ({
                          ...prev,
                          address: e.target.value,
                        }))
                      }
                      className="input input-bordered w-full"
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.col}>
              <div className={styles.row}>
                <div className={`${styles.rowTitle} ${styles.single}`}>
                  PickUp Date
                </div>
              </div>
              <div className={styles.row}>
                <div
                  className={`${styles.rowValue} ${styles.single} space-y-2 p-2`}
                >
                  <div className="">
                    <span className="text-sm text-gray-500">
                      Ready to Pickup At
                    </span>
                    <input
                      type="text"
                      placeholder="Contact Person"
                      disabled
                      value={dayjs(pickUp.startTime).format('DD-MM-YYYY')}
                      // onChange={(e) =>
                      //   setPickUp((prev) => ({
                      //     ...prev,
                      //     startTime: e.target.value,
                      //   }))
                      // }
                      className="input input-bordered w-full"
                    />
                  </div>
                  <div className="">
                    <span className="text-sm text-gray-500">Until</span>
                    <input
                      type="text"
                      placeholder="Phone"
                      disabled
                      value={dayjs(pickUp.endTime).format('DD-MM-YYYY')}
                      // value={pickUp.endTime}
                      // onChange={(e) =>
                      //   setPickUp((prev) => ({
                      //     ...prev,
                      //     endTime: e.target.value,
                      //   }))
                      // }
                      className="input input-bordered w-full"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Parcels */}
        <div className={styles.row}>
          <div className={`${styles.rowTitle} ${styles.single}`}>Parcels</div>
        </div>
        <div className="w-full">
          <div className="overflow-x-auto">
            <table className="table table-compact w-full">
              <thead>
                <tr>
                  <th>Parcel Ref ID</th>
                  <th>Weight(kg)</th>
                  <th>Height(cm)</th>
                  <th>Depth(cm)</th>
                  <th>Width(cm)</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {parcels.map((parcel, i) => {
                  return (
                    <tr key={i}>
                      <td className="w-4/12 text-left pl-2">{parcel.refId}</td>
                      <td className="w-2/12">{parcel.weight?.value}</td>
                      <td className="w-2/12">{parcel.dimension?.height}</td>
                      <td className="w-2/12">{parcel.dimension?.depth}</td>
                      <td className="w-2/12">{parcel.dimension?.width}</td>
                      <td>
                        <button
                          className="btn btn-error btn-xs"
                          onClick={() => onParcelDelete(i)}
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  )
                })}
                <tr ref={parcelEl}>
                  <td className="w-4/12 text-left pl-2">
                    <input
                      id="parcelId"
                      type="text"
                      placeholder="Parcel Ref ID"
                      className="input input-sm input-bordered w-full"
                    />
                  </td>
                  <td className="w-2/12">
                    <input
                      id="weight"
                      type="number"
                      placeholder="Weight(kg)"
                      className="input input-sm input-bordered w-full"
                    />
                  </td>
                  <td className="w-2/12">
                    {' '}
                    <input
                      id="height"
                      type="number"
                      placeholder="height(cm)"
                      className="input input-sm input-bordered w-full"
                    />
                  </td>
                  <td className="w-2/12">
                    <input
                      id="depth"
                      type="number"
                      placeholder="Depth(cm)"
                      className="input input-sm input-bordered w-full"
                    />
                  </td>
                  <td className="w-2/12">
                    <input
                      type="number"
                      id="width"
                      placeholder="Width(cm)"
                      className="input input-sm input-bordered w-full"
                    />
                  </td>
                  <td>
                    <button
                      className="btn btn-xs btn-primary w-full"
                      onClick={onParcelAdd}
                    >
                      Add
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <hr />

        <div>
          <h2 className="text-3xl text-gray-400 text-center">Order Detail</h2>
          <OrderDetail order={order} />
        </div>

        <hr />

        {/* Ready to submit */}
        <div className="flex justify-between items-center pb-5">
          <div className="flex-grow">
            {/* {error && (
              <span className="text-red-500 text-lg">
                <FontAwesomeIcon icon={faWarning} className="mr-4" />
                {error}
              </span>
            )} */}
          </div>
          <button className="btn btn-xl btn-primary" onClick={onSubmit}>
            Create Shipment
          </button>
        </div>
      </div>
    </Modal>
  )
}

export default NewShipmentModal
