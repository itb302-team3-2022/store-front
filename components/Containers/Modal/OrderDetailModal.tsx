import { Modal } from '@/components'
import { EnumModalAnimate, Order } from '@/utils/common/type'
import OrderDetail from './OrderDetail'
import styles from './Common.module.scss'
import { orderBy } from 'lodash'
import dayjs from 'dayjs'

type Props = {
  order: Order
  onClose: (e?: any) => void
}

const OrderDetailModal: React.FC<Props> = ({ order, onClose }) => {
  const sortedTimeline = order.shipping
    ? orderBy(order.shipping.shipmentTimeline, ['time'], ['desc'])
    : []

  const showTimeline = !!order.shipping

  return (
    <Modal
      backdrop
      title="Customer Order"
      animateStyle={EnumModalAnimate.FromBottom}
      onClose={onClose}
    >
      <div
        className="w-full sm:w-96 p-5 flex flex-col space-y-5"
        style={{ width: '80vw', height: '80vh' }}
      >
        <div>
          <h2 className={styles.title}>Order Detail</h2>
          <OrderDetail order={order} />
        </div>

        <hr />

        {showTimeline && (
          <div className="pb-20">
            <h2 className={styles.title}>Shipment</h2>
            <div className={styles.split}>
              <div className={styles.col}>
                <div className={styles.row}>
                  <div className={`${styles.rowTitle}`}>Track ID</div>
                  <div className={`${styles.rowValue}`}>
                    {order.shipping.trackId}
                  </div>
                </div>
                <div className={styles.row}>
                  <div className={`${styles.rowTitle}`}>Pickup from</div>
                  <div className={`${styles.rowValue} text-gray-700`}>
                    <div>
                      <p>{order.shipping.shipFrom.name}</p>
                      <p>{order.shipping.shipFrom.phone}</p>
                      <p>{order.shipping.shipFrom.address}</p>
                    </div>
                  </div>
                </div>

                <div className={styles.row}>
                  <div className={`${styles.rowTitle}`}>Ship to</div>
                  <div className={`${styles.rowValue} text-gray-700`}>
                    <div>
                      <p>{order.shipping.shipTo.receiver}</p>
                      <p>{order.shipping.shipTo.phone}</p>
                      <p>{order.shipping.shipTo.address}</p>
                    </div>
                  </div>
                </div>

                <div className={styles.row}>
                  <div className={`${styles.rowTitle}`}>Parcels</div>
                  <div className={`${styles.rowValue}`}>
                    <ul>
                      {order.shipping.parcels.map((item, i) => {
                        return (
                          <li className="text-gray-500 text-sm" key={i}>
                            <code>{item.refId}</code>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                </div>

                <div className={styles.row}>
                  <div className={`${styles.rowTitle}`}>Shipment Charge</div>
                  <div className={`${styles.rowValue}`}>${order.shipping.charge}</div>
                </div>
              </div>

              <div className={styles.col}>
                <div className={styles.row}>
                  <div className={`${styles.rowTitle} ${styles.single}`}>
                    Shipment Timeline
                  </div>
                </div>
                <div className={styles.row}>
                  <div
                    className={`${styles.rowValue} ${styles.single} relative`}
                  >
                    <ul className={styles.timelineContainer}>
                      {sortedTimeline.map((s, i) => {
                        const isHighlight = i === 0
                        const isPending = s.eventStatus === 'pending'
                        const highlightStyle = isHighlight
                          ? isPending
                            ? 'text-error'
                            : 'text-primary'
                          : ''
                        return (
                          <li key={i} className="flex mt-4">
                            <div className={`w-20`}>
                              <span
                                className={`${styles.statusPill} ${
                                  isHighlight ? styles.hightlight : ''
                                } ${isPending ? styles.pending : ''}`}
                              >
                                {s.eventStatus.toUpperCase()}
                              </span>
                            </div>
                            <div className="flex flex-col">
                              <p className={`${highlightStyle} text-sm`}>
                                {dayjs(s.time).format('DD-MM-YYYY HH:mm A')}
                              </p>
                              <p className={`${highlightStyle}`}>{s.title}</p>
                              <p className={styles.statusDesc}>{s.message}</p>
                            </div>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </Modal>
  )
}

export default OrderDetailModal
