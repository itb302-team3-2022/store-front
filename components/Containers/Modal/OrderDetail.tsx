import { Order } from '@/utils/common/type'
import { getCustomerFullName, getShortID } from '@/utils/helper'
import styles from './Common.module.scss'

type Props = {
  order: Order
}

const OrderDetail: React.FC<Props> = ({ order }) => {
  return (
    <div className={styles.split}>
      {/* Order Detail */}
      <div className={styles.col}>
        <div className={styles.row}>
          <div className={styles.rowTitle}>Order ID</div>
          <div className={styles.rowValue}>{getShortID(order._id)}</div>
        </div>
        <div className={styles.row}>
          <div className={styles.rowTitle}>Customer Name</div>
          <div className={styles.rowValue}>
            {getCustomerFullName(order.customer)}
          </div>
        </div>
        <div className={styles.row}>
          <div className={styles.rowTitle}>Customer Email</div>
          <div className={styles.rowValue}>{order.customer.email}</div>
        </div>
        <div className={styles.row}>
          <div className={`${styles.rowTitle} ${styles.single}`}>
            Purchased Items
          </div>
        </div>
        <div className={styles.row}>
          <div className={`${styles.rowValue} ${styles.single}`}>
            <table className="table table-compact w-full">
              <thead>
                <tr>
                  <th></th>
                  <th>ID</th>
                  <th>Item</th>
                  <th>Qty</th>
                  <th>Unit Price</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                {order.items.map((item, i) => {
                  return (
                    <tr key={i}>
                      <td className="font-bold">{i + 1}</td>
                      <td>{getShortID(item._id)}</td>
                      <td>{item.productId.name}</td>
                      <td>{item.qty}</td>
                      <td>{item.price}</td>
                      <td>{item.price * item.qty}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
        <div className={styles.row}>
          <div
            className={`${styles.rowValue} ${styles.single} flex justify-between font-bold`}
          >
            <span>Total:</span>
            <span>$ {order.totalAmount}</span>
          </div>
        </div>
      </div>
      <div className={styles.col}>
        <div className={styles.row}>
          <div className={`${styles.rowTitle} ${styles.single}`}>
            Shipping Address
          </div>
        </div>
        <div className={styles.row}>
          <div className={`${styles.rowValue} ${styles.single}`}>
            <label className="label">
              {/* <span className="label-text">Receiver</span> */}
              <span className="label-text-alt">Receiver</span>
            </label>
            <input
              type="text"
              disabled
              value={order.customer.delivery.receiver}
              className="input input-bordered w-full"
            />
            <label className="label">
              <span className="label-text-alt">Phone</span>
            </label>
            <input
              type="text"
              disabled
              value={order.customer.delivery.phone}
              className="input input-bordered w-full"
            />
            <label className="label">
              <span className="label-text-alt">Address</span>
            </label>
            <input
              type="text"
              disabled
              value={order.customer.delivery.address}
              className="input input-bordered w-full"
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default OrderDetail
