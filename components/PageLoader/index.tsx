import Portal from '@/HOC/Portal'
import { RootState } from '@/store'
import { AnimatePresence } from 'framer-motion'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { useSelector } from 'react-redux'

const PageLoader = () => {
const router = useRouter()
  const staff = useSelector((state: RootState) => state.app.staff)
  const show = !staff && router.pathname.includes('dashboard')

  return (
    <Portal>
      <AnimatePresence>
        {show && (
          <div className="fixed top-0 bottom-0 left-0 right-0 flex items-center justify-center bg-gray-800 z-50">
            <div
              style={{ borderTopColor: 'transparent' }}
              className="w-16 h-16 border-4 border-white-400 border-solid rounded-full animate-spin"
            ></div>
          </div>
        )}
      </AnimatePresence>
    </Portal>
  )
}

export default PageLoader
