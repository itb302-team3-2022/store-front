import { Dispatch } from '@/store'
import { EnumMessageType } from '@/utils/common/type'
import { Auth } from 'layouts'
import { useRouter } from 'next/router'
import { ChangeEvent, ReactElement, useState } from 'react'
import { useDispatch } from 'react-redux'

export default function Login() {
  const router = useRouter()
  const [input, setInput] = useState({
    username: '',
    password: '',
  })
  const dispatch = useDispatch<Dispatch>()

  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }))
  }

  const onSubmit = async () => {
    if (!input.username || !input.password) {
      dispatch.app.setAlertMessageAsync({
        type: EnumMessageType.Warning,
        text: 'Please input username and password'
      })
      return
    }
    
    try {
      const resp = await dispatch.app.login(input)
      router.push('dashboard')
    } catch (err) {}
  }

  return (
    <>
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full lg:w-6/12 px-4">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
              <div className="rounded-t mb-0 px-6 py-6">
                <div className="text-center mb-3">
                  <h2 className="text-blueGray-500 text-4xl font-bold mb-2">
                    Login
                  </h2>
                </div>

                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Username
                  </label>
                  <input
                    type="username"
                    name="username"
                    value={input.username}
                    onChange={onInputChange}
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    placeholder="Username"
                  />
                </div>

                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    value={input.password}
                    onChange={onInputChange}
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    placeholder="Password"
                  />
                </div>

                <div className="text-center mt-6">
                  <button className="btn btn-primary w-full" onClick={onSubmit}>
                    Login
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

Login.getLayout = (page: ReactElement) => <Auth>{page}</Auth>
