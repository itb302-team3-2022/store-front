import Link from 'next/link'

const NotFound = () => {
  return (
    <div className="w-screen h-screen flex flex-col justify-center items-center">
      <div className="text-9xl text-bold text-gray-700">404</div>
      <Link href="/">
        <span className="text-3xl cursor-pointer hover:text-blue-500">
          You may visit here
        </span>
      </Link>
    </div>
  )
}

export default NotFound
