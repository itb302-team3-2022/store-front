import { Dashboard } from 'layouts'
import { useRouter } from 'next/router'
import { ReactElement, useEffect } from 'react'

const DashboardPage = () => {
  const router = useRouter()
  useEffect(() => {
    router.push('/dashboard/orders')
  }, [])

  return <div>DashboardPage main</div>
}

export default DashboardPage

DashboardPage.getLayout = (page: ReactElement) => <Dashboard>{page}</Dashboard>
