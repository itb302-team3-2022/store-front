import { Dispatch, RootState } from '@/store'
import { Product } from '@/utils/common/type'
import { stringIncludes } from '@/utils/helper'
import { Dashboard } from 'layouts'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const ProductsPage = () => {
  const products = useSelector((state: RootState) => state.team3Store.products)
  const dispatch = useDispatch<Dispatch>()

  const [filteredProducts, setFilteredProducts] = useState<Product[]>(products)
  const [search, setSearch] = useState<string>('')

  useEffect(() => {
    dispatch.team3Store.fetchAllProducts()
  }, [])

  useEffect(() => {
    if (search) {
      const _filteredProducts = products.filter((p, index) => {
        const productId = p._id.substring(16)
        if (
          stringIncludes(productId, search) ||
          stringIncludes(p.name, search) ||
          stringIncludes(p.price.toString(), search) ||
          stringIncludes(p.description, search)
        ) {
          return p
        }
      })
      setFilteredProducts(_filteredProducts)
      return
    }

    setFilteredProducts(products)
  }, [search, products])

  return (
    <div>
      <input
        type="text"
        placeholder="Search (ID / Product Name / Pirce / Order Date)"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        className="input input-sm w-full mb-4"
      />

      <div className="overflow-x-auto w-full">
        <table className="table hover w-full">
          <thead>
            <tr className='text-center'>
              <th>PID</th>
              <th>Name</th>
              <th>Price</th>
            </tr>
          </thead>

          <tbody>
            {filteredProducts.map(
              ({ _id, name, images, description, price }) => {
                return (
                  <tr key={_id} className="hover">
                    <td className='text-sm'>{_id.substring(16)}</td>
                    <td>
                      <div className="flex items-center space-x-3">
                        <div className="avatar">
                          <div className="mask mask-squircle w-12 h-12">
                            <img src={images[0]} alt="product_image" />
                          </div>
                        </div>
                        <div>
                          <div className="font-bold">{name}</div>
                          <div className="text-sm opacity-50">
                            {description}
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>{price}</td>
                  </tr>
                )
              }
            )}
          </tbody>
          {/* <tfoot>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Price</th>
            </tr>
          </tfoot> */}
        </table>
      </div>
    </div>
  )
}

export default ProductsPage

ProductsPage.getLayout = (page: ReactElement) => <Dashboard>{page}</Dashboard>
