import { Dispatch, RootState } from '@/store'
import { EnumModalAnimate, Order } from '@/utils/common/type'
import { Dashboard } from 'layouts'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  getCustomerFullName,
  getDate,
  getShortID,
  getStatusStyle,
  getTime,
  stringIncludes,
} from '@/utils/helper'
import { Modal, Pill } from '@/components'
import styles from 'styles/pages/Dashboard.module.scss'
import Portal from '@/HOC/Portal'
import { NewShipmentModal, OrderDetailModal } from '@/components/Containers'
import { AnimatePresence } from 'framer-motion'

enum EnumModal {
  NewShipment = 'new-shipment',
  // ViewShipment = 'view-shipment',
  OrderDetail = 'order-detail',
}

type ModalStates = {
  [key in EnumModal]: Order | null
}

const ProductsPage = () => {
  const orders = useSelector((state: RootState) => state.team3Store.orders)
  const initModalStates: ModalStates = {
    [EnumModal.NewShipment]: null,
    [EnumModal.OrderDetail]: null,
  }
  const [modalStates, setModalStates] = useState<ModalStates>(initModalStates)
  // const [activeOrder, setActiveOrder] = useState<Order | null>(null)
  const dispatch = useDispatch<Dispatch>()

  const [filteredItems, setFilteredItems] = useState<Order[]>(orders)
  const [search, setSearch] = useState<string>('')

  const onClick = (modal: EnumModal, id?: string) => {
    if (id) {
      const order = orders.find((o) => o._id === id) as Order
      setModalStates((prev) => ({ ...prev, [modal]: order }))
      return
    }
    setModalStates(initModalStates)
  }

  const onButtonClick = (e: any, id: string) => {
    e.stopPropagation()
    switch (e.target.id) {
      case 'create-shipment':
        onClick(EnumModal.NewShipment, id)
        break
      default:
        onClick(EnumModal.OrderDetail, id)
        break
    }
  }

  useEffect(() => {
    dispatch.team3Store.fetchAllOrders()
  }, [])

  useEffect(() => {
    if (search) {
      // const _search = search.toLowerCase()
      const _filteredProducts = orders.filter((o, index) => {
        const orderDate = getDate(o.createdAt)

        if (
          (o.shipping && stringIncludes(o.shipping.trackId, search)) ||
          stringIncludes(getShortID(o._id), search) ||
          stringIncludes(o.customer.firstName, search) ||
          stringIncludes(o.customer.lastName, search) ||
          stringIncludes(
            o.customer.lastName + ' ' + o.customer.firstName,
            search
          ) ||
          stringIncludes(
            o.customer.firstName + ' ' + o.customer.lastName,
            search
          ) ||
          stringIncludes(o.customer.email, search) ||
          stringIncludes(orderDate, search) ||
          stringIncludes(o.status, search) ||
          stringIncludes(o.paymentRef, search)
        ) {
          return o
        }
      })
      setFilteredItems(_filteredProducts)
      return
    }
    setFilteredItems(orders)
  }, [search, orders])

  return (
    <div>
      <input
        type="text"
        placeholder="Search (ID / Customer Name / Status / Order Date / TrackID / Payment Ref)"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        className="input input-sm w-full mb-4"
      />

      <div className="overflow-x-auto w-full">
        <table className="table hover w-full">
          <thead>
            <tr>
              <th>ID</th>
              <th>Customer</th>
              <th>Order Date</th>
              <th>Payment Ref</th>
              <th>Status</th>
              <th>TrackID</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>
            {filteredItems.map((item, i) => {
              const isNewOrder = !item.shipping

              return (
                <tr
                  key={i}
                  className="hover hover:cursor-pointer"
                  onClick={(e) => onButtonClick(e, item._id)}
                >
                  <td className="text-sm">{item._id.substring(16)}</td>
                  <td>
                    <div>
                      <div className="font-bold">
                        {item.customer.firstName} {item.customer.lastName}
                      </div>
                      <div className="text-sm opacity-50">
                        {item.customer.email}
                      </div>
                    </div>
                  </td>
                  <td>
                    <div>{getDate(item.createdAt)}</div>
                    <div className="text-sm opacity-50">
                      {getTime(item.createdAt)}
                    </div>
                  </td>
                  <td>{item.paymentRef}</td>
                  <td>
                    <Pill
                      className={getStatusStyle(item.status)}
                      text={item.status.toUpperCase()}
                    />
                  </td>
                  <td className="pointer-events-none">
                    {item.shipping?.trackId}
                  </td>
                  <td>
                    {isNewOrder && (
                      <button
                        className="btn btn-secondary capitalize btn-sm pointer-events-auto"
                        id="create-shipment"
                        onClick={(e) => onButtonClick(e, item._id)}
                      >
                        Create Shipment
                      </button>
                    )}
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>

      <Portal>
        <AnimatePresence>
          {modalStates['new-shipment'] && (
            <NewShipmentModal
              order={modalStates['new-shipment']}
              onClose={() => onClick(EnumModal.NewShipment)}
            />
          )}
          {modalStates['order-detail'] && (
            <OrderDetailModal
              order={modalStates['order-detail']}
              onClose={() => onClick(EnumModal.OrderDetail)}
            />
          )}
        </AnimatePresence>
      </Portal>
    </div>
  )
}

export default ProductsPage

ProductsPage.getLayout = (page: ReactElement) => <Dashboard>{page}</Dashboard>
