import { Modal } from '@/components'
import Portal from '@/HOC/Portal'
import { Dispatch, RootState } from '@/store'
import { Customer, Product } from '@/utils/common/type'
import { stringIncludes } from '@/utils/helper'
import { Dashboard } from 'layouts'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const ProductsPage = () => {
  const customers = useSelector(
    (state: RootState) => state.team3Store.customers
  )
  const dispatch = useDispatch<Dispatch>()

  const [filteredItems, setFilteredItems] = useState<Customer[]>(customers)
  const [search, setSearch] = useState<string>('')

  useEffect(() => {
    dispatch.team3Store.fetchAllCustomers()
  }, [])

  useEffect(() => {
    if (search) {
      const _search = search.toLowerCase()
      const _filteredItems = customers.filter((item, index) => {
        const { _id, email, firstName, lastName, delivery } = item
        const customerId = _id.substring(16)
        if (
          stringIncludes(customerId, search) ||
          stringIncludes(firstName, search) ||
          stringIncludes(lastName, search) ||
          stringIncludes(email, search) ||
          stringIncludes(delivery.phone, search) ||
          stringIncludes(delivery.phone2, search) ||
          stringIncludes(delivery.address, search)
        ) {
          return item
        }
      })
      setFilteredItems(_filteredItems)
      return
    }

    setFilteredItems(customers)
  }, [search, customers])

  return (
    <div>
      <input
        type="text"
        placeholder="Search (ID / Name / Email / Phone / Address )"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        className="input input-sm w-full mb-4"
      />

      <div className="overflow-x-auto w-full">
        <table className="table hover w-full">
          <thead>
            <tr className="text-center">
              <th>ID</th>
              <th>Name</th>
              <th>phone</th>
              <th>orders</th>
            </tr>
          </thead>

          <tbody>
            {filteredItems.map(
              ({ _id, firstName, lastName, email, delivery, order }, i) => {
                return (
                  <tr key={_id}>
                    <td className='text-sm'>{_id.substring(16)}</td>
                    <td>
                      <div className="flex items-center space-x-3">
                        <div className="font-bold">
                          {firstName}{' '}
                          <span className="font-normal">{lastName}</span>
                        </div>
                        <div className="text-sm opacity-50">{email}</div>
                      </div>
                    </td>
                    <td className='text-center'>{delivery.phone}</td>
                    <td className='text-center'>{order.length}</td>
                  </tr>
                )
              }
            )}
          </tbody>
          {/* <tfoot>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Price</th>
              <th>Orders</th>
            </tr>
          </tfoot> */}
        </table>
      </div>
    </div>
  )
}

export default ProductsPage

ProductsPage.getLayout = (page: ReactElement) => <Dashboard>{page}</Dashboard>
