import { Alert } from '@/components'
import PageLoader from '@/components/PageLoader'
import { AnimatePresence } from 'framer-motion'
import type { AppProps } from 'next/app'
import { NextPage } from 'next/types'
import { ReactElement, ReactNode } from 'react'
import { Provider } from 'react-redux'
import { store } from 'store'
import 'styles/globals.css'

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page)
  return getLayout(
    <Provider store={store}>
      <Component {...pageProps} />
      <AnimatePresence>
        <Alert />
      </AnimatePresence>
      <PageLoader />
    </Provider>
  )
}

export default MyApp
