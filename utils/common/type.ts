/**
 * All Enum
 *
 */
export enum EnumWeightUnit {
  Kg = 'kg',
  Lb = 'lb',
  G = 'g',
}

export enum EnumDimensionUnit {
  Cm = 'cm',
  Ft = 'ft',
  Inch = 'inch',
  M = 'm',
}

export enum EnumCurrency {
  Hkd = 'hkd',
  Usd = 'usd',
  Rmb = 'rmb',
}

export enum EnumShipmentStatus {
  Pending = 'pending',
  Preparing = 'preparing',
  Picking = 'picking',
  Shipping = 'shipping',
  OnTheWay = 'on the way',
  Waiting = 'waiting',
  Shipped = 'shipped',
}

export enum EnumOrderStatus {
  Pending = 'pending',
  Processing = 'processing',
  Completed = 'completed',
  Rejected = 'rejected',
}

export enum EnumModalAnimate {
  FromBottom = 'bottom',
  FromTop = 'top',
  FromLeft = 'left',
  FromRight = 'right',
  ZoomIn = 'zoomin',
  ZoomOut = 'zoomout',
}

export enum EnumMessageType {
  Success,
  Fail,
  Warning,
}

export type ShipFrom = {
  name: string
  phone: string
  phone2?: string
  address: string
}

export type ShipTo = {
  receiver: string
  phone: string
  phone2?: string
  address: string
}

export type Parcel = {
  refId: string
  weight?: {
    value: number
    unit: EnumWeightUnit
  }
  dimension?: {
    depth: number
    width: number
    height: number
    unit: EnumDimensionUnit
  }
  description: string
  insuredValue?: {
    amount: number
    currency: EnumCurrency
  }
}

export type PickUp = {
  startTime: number // timestamp,
  endTime: number
}

export type Delivery = {
  receiver: string
  phone: string
  phone2: string
  address: string
  _id: string
}

export type Customer = {
  _id: string
  firstName: string
  lastName: string
  email: string
  order: Order[]
  delivery: Delivery
}

export type OrderItems = {
  productId: Product
  qty: number
  price: number
  _id: string
}

export type ShipmentTimeline = {
  time: string
  title: string
  message: string
  eventStatus: EnumShipmentStatus
  _id: string
}

export type Product = {
  _id: string
  name: string
  images: string[]
  description: string
  price: string
  createdAt: string
  updatedAt: string
}

export type Order = {
  _id: string
  totalAmount: number
  customer: Customer
  items: OrderItems[]
  paymentRef: string
  delivery: Delivery
  status: EnumOrderStatus
  createdAt: string
  updatedAt: string
  shipping: {
    shipFrom: ShipFrom
    shipTo: ShipTo
    pickUp: PickUp
    trackId: string
    parcels: Parcel[]
    charge: number
    status: EnumShipmentStatus
    shipmentTimeline: ShipmentTimeline[]
    _id: string
  }
}

export type AlertMessage = {
  type: EnumMessageType
  text: string
}
export type Staff = {
  firstname: string
  _id: string
}

/**
 * Redux state
 */

export type Team3StoreState = {
  orders: Order[]
  products: Product[]
  customers: Customer[]
}

export type AppState = {
  alertMessage: AlertMessage | null
  staff: Staff | null
}
