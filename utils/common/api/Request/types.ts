import { Parcel, PickUp, ShipFrom } from '../../type'

export type NewShipmentData = {
  orderId: string
  shipFrom: ShipFrom
  parcels: Parcel[]
  pickUp: PickUp
}

export type LoginData = {
  username: string
  password: string
}
