import { CancelToken, Method } from 'axios'

export interface Config {
  url: string
  method: Method
  params?: Data
  data?: any
  headers?: any
  transformResponse?: any
  cancelToken?: CancelToken
  paramsSerializer?: (params: any) => any
  validateStatus?: ((status: number) => boolean) | null
  withCredentials: boolean
}

export interface Headers {
  [key: string]: string
}

export interface Data {
  [key: string]: any
}

export interface Options {
  cancelToken?: CancelToken | null
  authorize?: boolean | null
}
